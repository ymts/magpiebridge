package org.jiucheng.magpiebridge.server.aio;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * 
 * @author jiucheng
 *
 */
public class ServerEstablishmentCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, AsynchronousServerSocketChannel> {

    public void completed(AsynchronousSocketChannel result, AsynchronousServerSocketChannel attachment) {
    	attachment.accept(attachment, this);
        
        ByteBuffer readBuffer = ByteBuffer.allocate(13);
        result.read(readBuffer, new ServerAttachment(result).setReadBuffer(readBuffer), new ServerReadCompletionHandler());
    }

    public void failed(Throwable exc, AsynchronousServerSocketChannel attachment) {
        exc.printStackTrace();
    }
}
