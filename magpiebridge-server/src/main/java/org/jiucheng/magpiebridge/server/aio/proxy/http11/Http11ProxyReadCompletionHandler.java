package org.jiucheng.magpiebridge.server.aio.proxy.http11;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.util.List;

import org.jiucheng.magpiebridge.protocol.Message;
import org.jiucheng.magpiebridge.server.aio.ServerAttachment;
import org.jiucheng.magpiebridge.server.aio.ServerWriteNoProxyClientReadCompletionHandler;
import org.jiucheng.magpiebridge.server.aio.proxy.ProxyAttachment;
import org.jiucheng.magpiebridge.server.util.Container;

public class Http11ProxyReadCompletionHandler implements CompletionHandler<Integer, Http11ProxyAttachment> {

	public void completed(Integer result, Http11ProxyAttachment attachment) {
		if (result != -1) {
			ByteBuffer readBuffer = attachment.getReadBuffer();
	        byte[] data = new byte[readBuffer.position()];
	        readBuffer.flip();
	        readBuffer.get(data);
	        String[] lines = new String(data).split("\r\n");
	        if (!lines[0].endsWith("HTTP/1.1")) {
	        	attachment.close();
	        }
	        for (int i = 1; i < lines.length; i++) {
	        	String line = lines[i];
	        	if (line.startsWith("Host: ")) {
	        		// 域名
	        		String domain = line.substring(6);
					List<Object> item = Container.DOMAINS.get(domain);
					if (item != null) {
						ServerAttachment serverAttachment = (ServerAttachment) item.get(0);
						String remote = (String) item.get(1);
				        if (serverAttachment.canWrited()) {
				            Message message = new Message();
				            message.setMagic(Message.MAGIC);
				            message.setType(Message.Type.CONNECT);
				            message.setUri(Container.CLIENTS_ID.incrementAndGet());
				            byte[] datas = remote.getBytes();
				            message.setData(datas);
				            message.setSize(datas.length);
				            ByteBuffer writeBuffer = Message.toByteBuffer(message);
				            
				            readBuffer.position(data.length);
				            ServerAttachment.proxys.put(message.getUri(), new ProxyAttachment(attachment.getServer()).setReadBuffer(readBuffer).setUri(message.getUri()).setServerAttachment(serverAttachment));
				            serverAttachment.getServer().write(writeBuffer, serverAttachment.setWriteBuffer(writeBuffer), new ServerWriteNoProxyClientReadCompletionHandler());
				        }
						return;
					}
	        		break;
	        	}
	        }
		}
        attachment.close();
	}

	public void failed(Throwable exc, Http11ProxyAttachment attachment) {
		attachment.close();
	}
}
